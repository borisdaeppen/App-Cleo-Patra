package App::Cleo::Patra;

use strict;
use warnings;

use Term::ReadKey;
use Term::ANSIColor qw(colored);
use File::Slurp qw(read_file);
use Time::HiRes qw(usleep);

use constant PS1 => 'ps1';
use constant PS2 => 'ps2';
our $VERSION = 0.001;

#-----------------------------------------------------------------------------

sub new {
    my $class = shift;

    my $self = {
        shell  => $ENV{SHELL} || '/bin/bash',
        chldpid=> 0,
        ps1    => colored( ['green'], '(%d)$ '),
        ps2    => colored( ['green'], '> '),
        delay  =>  25_000,
        sleep  => 100_000,
        state  => PS1,
        current_cmd => -1,
        @_,
    };

    return bless $self, $class;
}

#-----------------------------------------------------------------------------

sub run {
    my ($self, $input, $multiline) = @_;

    #my $type = ref $input;
    #my @commands = !$type ? read_file($input)
    #    : $type eq 'SCALAR' ? split "\n", $input
    #        : $type eq 'ARRAY' ? @{$input}
    #            : die "Unsupported type: $type";

    my @commands = ();
    if (!$type) {
        if ($multiline) {
            my $data = read_file($input);
            @commands = split /^\$\s/m, $data;
        }
        else {
            @commands = read_file($input);
        }
    }
    elsif ($type eq 'SCALAR') {
        @commands = split "\n", $input;
    }
    elsif (type eq 'ARRAY' {
        @commands = @{$input};
    }
    else {
        die "Unsupported type: $type";
    }

    $self->{chldpid} = open my $fh, '|-', $self->{shell} or die $!;
    $self->{fh}  = $fh;
    ReadMode('raw');
    local $| = 1;

    local $SIG{CHLD} = sub {
        print $self->app_error("child shell exited during command $self->{current_cmd}");
        #ReadMode('restore');
        #exit;
    };

    chomp @commands;
    @commands = grep { /^\s*[^\#;]\S+/ } @commands;
    @commands = grep { /.+/ } @commands if $multiline;

    my $continue_to_end = 0;

    CMD:
    for (my $i = 0; $i < @commands; $i++) {

        my $cmd = defined $commands[$i] ? $commands[$i] : die "no command $i";
        chomp $cmd;
        $self->{current_cmd} = $i;

        my $keep_going = $cmd =~ s/^\.\.\.//;
        my $run_in_background = $cmd =~ s/^!!!//;

        $self->do_cmd($cmd) and next CMD
            if $run_in_background;

        no warnings 'redundant';
        my $prompt_state = $self->{state};
        print sprintf $self->{$prompt_state}, $i;

        my @steps = split /%%%/, $cmd;
        while (my $step = shift @steps) {

            my $should_pause = !($keep_going || $continue_to_end);
            my  $key  = $should_pause ? ReadKey(0) : '';
            if ($key  =~ /^\d$/) {
                $key .= $1 while (ReadKey(0) =~ /^(\d)/);
            }
            print "\n" if $key =~ m/^[srp]|[0-9]+/;

            last CMD             if $key eq 'q';
            next CMD             if $key eq 's';
            redo CMD             if $key eq 'r';
            $i--, redo CMD       if $key eq 'p';
            $i = $key, redo CMD  if $key =~ /^\d+$/;
            $continue_to_end = 1 if $key eq 'c';

            $step .= ' ' if not @steps;
            my @chars = split '', $step;
            print and usleep $self->{delay} for @chars;
        }

        my $should_pause = !($keep_going || $continue_to_end);
        my  $key  = $should_pause ? ReadKey(0) : '';
        if ($key  =~ /^\d$/) {
            $key .= $1 while (ReadKey(0) =~ /^(\d)/);
        }
        print "\n";

        last CMD             if $key eq 'q';
        next CMD             if $key eq 's';
        redo CMD             if $key eq 'r';
        $i--, redo CMD       if $key eq 'p';
        $i = $key, redo CMD  if $key =~ /^\d+$/;
        $continue_to_end = 1 if $key eq 'c';

        $self->do_cmd($cmd);
    }

    ReadMode('restore');
    print "\n";

    return $self;
}

#-----------------------------------------------------------------------------

sub do_cmd {
    my ($self, $cmd) = @_;

    my $cmd_is_finished;
    local $SIG{ALRM} = sub {$cmd_is_finished = 1};

    $cmd =~ s/%%%//g;
    my $fh = $self->{fh};

    print $fh "$cmd\n";

    ($self->{state} = PS2) and return 1
        if $cmd =~ m{\s+\\$};

    print $fh "kill -14 $$\n"; # SIG-ALRM
    $fh->flush;

    # Wait for signal that command has ended
    my $limit = 0;
    my $interrupt_key = '';
    until ($cmd_is_finished) {
        $limit++;
        usleep( $self->{sleep});
        $interrupt_key = ReadKey(-1) // ''; # non blocking key polling
        if ($limit >= 300 or $interrupt_key =~ /q/) {
            if ($limit >= 300) {
                my $waited = int($limit * $self->{sleep} / 1000000);
                print $self->app_error("waited $waited seconds, killing command");
            }
            elsif ( $interrupt_key =~ /q/) {
                print $self->app_error("caught quitting key, killing command");
            }
            kill 'KILL', $self->{chldpid};
            print $self->app_error("reopening shell");
            $self->{pid} = open $self->{fh}, '|-', $self->{shell} or die $!;
            $self->{fh}  = $fh;
            ReadMode('raw');
            local $| = 1;
            $cmd_is_finished = 1;
        }
    }
    $cmd_is_finished = 0;

    $self->{state} = PS1;

    return 1;
}

#-----------------------------------------------------------------------------

sub app_error {
    my ($self, $msg) = @_;

    return colored(['red'], ">>> $msg\n");
}

#-----------------------------------------------------------------------------
1;

=pod

=encoding utf8

=head1 NAME

App::Cleo - Play back shell commands for live demonstrations

=head1 SYNOPSIS

  use App::Cleo::Patra
  my $patra = App::Cleo::Patra->new(%options);
  $patra->run($commands);

=head1 DESCRIPTION

B<Important:>
C<patra> is an experimental fork from C<cleo>.
You should check the current differences from C<App-Cleo> and decide, which one you want to use.
It may be, that in your current time, C<patra> is merged back into C<cleo> or obsolete for other reasons.

App::Cleo::Patra is the back-end for the L<patra> utility.  Please see the L<patra>
documentation for details on how to use this.

=head1 CONSTRUCTOR

The constructor accepts arguments as key-value pairs.  The following keys are
supported:

=over 4

=item delay

Number of microseconds to wait before displaying each character of the command.
The default is C<25_000>.

=item ps1

String to use for the artificial prompt.  The token C<%d> will be substituted
with the number of the current command.  The default is C<(%d)$>.

=item ps2

String to use for the artificial prompt that appears for multiline commands. The
token C<%d> will be substituted with the number of the current command.  The
default is C<< > >>.

=item shell

Path to the shell command that will be used to run the commands.  Defaults to
either the C<SHELL> environment variable or C</bin/bash>.

=back

=head1 METHODS

=over 4

=item run( $commands )

Starts playback of commands.  If the argument is a string, it will be treated
as a file name and commands will be read from the file. If the argument is a
scalar reference, it will be treated as a string of commands separated by
newlines.  If the argument is an array reference, then each element of the
array will be treated as a command.

=back

=head1 AUTHOR

Jeffrey Ryan Thalhammer <thaljef@cpan.org>

Boris Däppeb (BORISD) <bdaeppen.perl@gmail.com>

=head1 COPYRIGHT

cleo - Copyright (c) 2014, Imaginative Software Systems

patra - Boris Däppen (BORISD) 2018

=cut
